package Array;

    import java.util.Scanner;

	public class Palindrom {

			public static void main(String[] args) {

				// char-Array mit 5 Elementen erstellen:
				char[] zeichen = new char[5];
				
				// Scanner-Objekt für die Konsoleneingabe erzeugen:
				Scanner tastatur = new Scanner(System.in);
				
				// Zeichen aus der Konsoleneingabe im Array speichern:
				for(int i = 0; i < zeichen.length; i++) {
					System.out.print((i + 1) + ". Zeichen: ");
					zeichen[i] = tastatur.next().charAt(0);
				}
				
				// Zeichen in umgekehrter Reihenfolge ausgeben:
				for(int i = zeichen.length - 1; i >= 0 ; i--) {
					System.out.print(zeichen[i]);
				}
				System.out.println();
			}
	}
