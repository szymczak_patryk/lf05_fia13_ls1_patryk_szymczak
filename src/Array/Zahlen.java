package Array;



public class Zahlen {

	public static void main(String[] args) {

		// Array deklarieren:
		int[] zahlenfeld = new int[10];
		
		// Zahlen mit Zahlen fūllen:
		for(int i = 0; i < zahlenfeld.length; i++) {
			zahlenfeld[i] = i;
		}
		
		// Array ausgeben:
		for(int i = 0; i < zahlenfeld.length; i++) {
			System.out.println(zahlenfeld[i] + " ");
			
			System.out.println();
		}
	}
}