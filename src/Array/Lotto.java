package Array;

public class Lotto {

public static void main(String[] args) {
		
		// Teil a)
		// ======
		// Erstellen Sie ein Array "lottoziehung",
		// welches 6 ganze Zahlen aufnehmen kann (Lottozahlen).
		int[] lottoziehung = new int[6];
		
		// Tragen Sie in die 6 Array-Elemente die Zahlen
		// 3, 7, 12, 18, 37 und 42 ein.
		lottoziehung[0] = 3;
		lottoziehung[1] = 7;
		lottoziehung[2] = 12;
		lottoziehung[3] = 18;
		lottoziehung[4] = 37;
		lottoziehung[5] = 42;
		
		// Geben Sie den Inhalt des Arrays mit Hilfe 
		// einer Wiederholungsanweisung in der folgenden Darstellung in der Konsole aus.
		// [  3  7  12  18  37  42  ]
		System.out.print("[  ");
		for(int i = 0; i < lottoziehung.length; i++) {
			System.out.print(lottoziehung[i] + "  ");
		}		
		System.out.println("]");
		
		// Teil b)
		// ======
		// Prüfen Sie nacheinander, ob die Zahl 12
		// bzw. die Zahl 13 in der Lottoziehung vorkommt.
		// Geben Sie aus
		// Die Zahl 12 ist in der Ziehung enthalten.
		// Die Zahl 13 ist nicht in der Ziehung enthalten.
		
		int gesuchteZahl;
		boolean istEnthalten;
		
		gesuchteZahl = 12;
		istEnthalten = false;
		for(int i = 0; i < lottoziehung.length; i++) {
		   if(lottoziehung[i] == gesuchteZahl) {
			   istEnthalten = true;
			   break;
		   }
		}
		if(istEnthalten) {
			   System.out.println("Die Zahl " + gesuchteZahl +  " ist in der Ziehung enthalten.");			
		}
		else {
			   System.out.println("Die Zahl " + gesuchteZahl +  " ist nicht in der Ziehung enthalten.");						
		}
		
		gesuchteZahl = 13;
		istEnthalten = false;
		for(int i = 0; i < lottoziehung.length; i++) {
		   if(lottoziehung[i] == gesuchteZahl) {
			   istEnthalten = true;
			   break;
		   }
		}
		if(istEnthalten) {
			System.out.println("Die Zahl " + gesuchteZahl +  " ist in der Ziehung enthalten.");			
		}
		else {
			System.out.println("Die Zahl " + gesuchteZahl +  " ist nicht in der Ziehung enthalten.");						
		}
	}
}

