package konsolenausgabe;

public class Aufgabe6 {
  public static void main(String[] args) {
	
    double c1 = -28.8889, c2 = -23.3333, c3 = -17.7778, c4 = -6.6667, c5 = -1.1111;
    int f1 = -20, f2 = -10, f3 = 0, f4 = 20, f5 = 30;
    
    //Kopfzeile
    System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");

    //Trennlinie
    System.out.printf("%23s\n", "-----------------------");
    
    //Werte
    System.out.printf("%-12d|%10.2f\n", f1, c1);
    System.out.printf("%-12d|%10.2f\n", f2, c2);
    System.out.printf("+%-11d|%10.2f\n", f3, c3);
    System.out.printf("+%-11d|%10.2f\n", f4, c4);
    System.out.printf("+%-11d|%10.2f", f5, c5); 
  }
}
