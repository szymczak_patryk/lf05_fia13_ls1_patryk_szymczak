package konsolenausgabe;

import java.util.Scanner;

public class Nutzerdaten {
	public static void main(String[] args) {
		Scanner my_scanner = new Scanner(System.in);
		
		System.out.print("Guten Tag, wie hei�en Sie?\n>> ");
		String name = my_scanner.next();
		
		System.out.print("\nHallo " + name + ". Wie alt sind Sie?\n>> ");
		int age = my_scanner.nextInt();
		
		System.out.println("\nSie hei�en also " + name + " und sind " + age + " Jahre alt.");
		
		my_scanner.close();
	}
}
