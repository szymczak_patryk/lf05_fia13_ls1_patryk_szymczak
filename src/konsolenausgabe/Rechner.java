package konsolenausgabe;

import java.util.Scanner;

public class Rechner {
	public static void main(String[] args) {
		Scanner my_scanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		int zahl1 = my_scanner.nextInt();
		
		System.out.print("\nBitte geben Sie eine zweite ganze Zahl ein: ");
		int zahl2 = my_scanner.nextInt();
		
		System.out.print("\nBitte geben Sie die Rechenoperation an (+; -, *, /): ");
		char op = my_scanner.next().charAt(0);
		
		switch (op) {
		case '+': System.out.println(zahl1 + " + " + zahl2 + " = " + (zahl1 + zahl2)); break;
		case '-': System.out.println(zahl1 + " - " + zahl2 + " = " + (zahl1 - zahl2)); break;
		case '*': System.out.println(zahl1 + " * " + zahl2 + " = " + (zahl1 * zahl2)); break;
		case '/': System.out.println(zahl1 + " / " + zahl2 + " = " + (zahl1 / zahl2)); break;
		default: System.out.println("\nSie haben keine g�ltige Rechenoperation eingegeben."); break;
		}
		
		my_scanner.close();
	}
}
