package methodenaufgaben;

import java.util.Scanner;

public class Aufgabe5 {
	public static void main(String[] args) {
		boolean running = true;

		System.out.println("---------------Reisebudget-Umrechner---------------");
		
		double gesamtbudget = eingabeBudget("Geben Sie Ihr gesamtes Budget f�r diese Reise ein: ");
		double untergrenze = eingabeUntergrenze(gesamtbudget, "Geben Sie Ihre Untergrenze des Budgets f�r diese Reise ein: ");
		
		while (running) {
			System.out.println("-----------------------------------------------------");
			
			gesamtbudget = rechneWahrung(gesamtbudget, untergrenze);
			
			if (gesamtbudget == untergrenze) {
				System.out.println("\nSie haben Ihre Untergrenze von " + untergrenze + " erreicht.\nTsch�ss!");
				break;
			}

			running = check("\nWollen Sie das Programm schlie�en? [Y/N]: ");
		}
	}

	public static double eingabeBudget(String text) {
		Scanner my_scanner = new Scanner(System.in);

		System.out.println("\n" + text);
		return my_scanner.nextDouble();
	}

	public static double eingabeUntergrenze(double budget, String text) {
		Scanner my_scanner = new Scanner(System.in);
		boolean i = true;
		double untergrenze = 0.0;

		do {
			System.out.println("\n" + text);
			untergrenze = my_scanner.nextDouble();

			if (untergrenze >= budget) {
				System.out.println("Sie k�nnen die Untergrenze des Budgets nicht gr��er oder gleich Ihres Gesamtbudgets festlegen!\n");
			} else {
				i = false;
			}
		} while (i);
		return untergrenze;
	}

	public static double rechneWahrung(double budget, double untergrenze) {
		Scanner my_scanner = new Scanner(System.in);
		boolean i = true;
		double betrag;

		System.out.println("\nGeben Sie das Gastland an!\n[USA, JAP, ENG, SWZ, SWD]: ");
		String country = my_scanner.next();

		do {
			System.out.println("\nGeben Sie den einzuwechselnden Betrag an: ");
			betrag = my_scanner.nextDouble();

			if ((budget - betrag) < untergrenze) {
				System.out.println("\nDas w�re mehr als Ihre Untergrenze von " + untergrenze + " Euro.");
			} else {
				i = false;
			}
		} while (i);

		budget -= betrag;

		switch (country) {
			case "USA": System.out.printf("\n%.2f Euro sind %.2f Dollar.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 1.22, budget); break;
			case "JAP": System.out.printf("\n%.2f Euro sind %.2f Yen.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 126.50, budget); break;
			case "ENG": System.out.printf("\n%.2f Euro sind %.2f Pfund.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 0.89, budget); break;
			case "SWZ": System.out.printf("\n%.2f Euro sind %.2f Schweizer Franken.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 1.08, budget); break;
			case "SWD": System.out.printf("\n%.2f Euro sind %.2f Schwedische Kronen.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 10.10, budget); break;
			case "usa": System.out.printf("\n%.2f Euro sind %.2f Dollar.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 1.22, budget); break;
			case "jap": System.out.printf("\n%.2f Euro sind %.2f Yen.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 126.50, budget); break;
			case "eng": System.out.printf("\n%.2f Euro sind %.2f Pfund.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 0.89, budget); break;
			case "swz": System.out.printf("\n%.2f Euro sind %.2f Schweizer Franken.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 1.08, budget); break;
			case "swd": System.out.printf("\n%.2f Euro sind %.2f Schwedische Kronen.\nSie haben nun noch ein Gesamtbudget von %.2f Euro.\n", betrag, betrag * 10.10, budget); break;
			default: break;
		}
		return budget;
	}

	public static boolean check(String text) {
		Scanner my_scanner = new Scanner(System.in);

		System.out.println(text);
		char check = my_scanner.next().charAt(0);

		if (check == 'Y' || check == 'y') {
			return false;
		} else {
			return true;
		}
	}
}
