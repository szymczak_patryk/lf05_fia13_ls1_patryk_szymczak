package auswahlstruktur;

import java.util.Scanner;

public class Schleifen2_Aufgabe6 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		char eingabe = 'n';
		double anlageKapital, zinssatz;
		int i = 1;

		while (eingabe == 'n') {

			
			System.out.print("Bitte geben Sie an, wie viel Kapital Sie anlegen möchten:");
			anlageKapital = sc.nextDouble();

			
			System.out.print("Bitte geben Sie den Zinssatz an:");
			zinssatz = sc.nextDouble();
			zinssatz = zinssatz / 100 + 1;

	
			while (anlageKapital < 1000000) {
				anlageKapital = anlageKapital * zinssatz;
				i++;
			}
			
			System.out.println("Nach " + i + " Jahren überschreitet das Anlagekapitel die Schwelle von 1.000.000 Euro");
			
			System.out.println("Möchten Sie einen weiteren Durchlauf starten? (j/n) ");
			eingabe = sc.next().charAt(0);
		}
		sc.close();
	}



	}

