package auswahlstruktur;

import java.util.Scanner;

public class Auswahlstrukturen_Aufgabe5 {

	public static void main(String[] args) {
		Scanner einlesen = new Scanner(System.in);  
	 
	    int wert, wert2; 
	    String ausgabe = "";
	    String eingabe;
	    
	    double kilogramm;
	    double kilogramm_bmi;
	    double gewicht; 
	    String geschlecht;
	    double bmi;
	    boolean männlich;
	    
	    eingabe="Bitte geben Sie Ihre Körpergröße in CM  an: ";
	    System.out.println("");
	    System.out.print(eingabe);
	    kilogramm = einlesen.nextDouble();
	    eingabe="Bitte geben Sie jetzt Ihr Gewicht in KG  an: ";
	    System.out.print(eingabe);
	    gewicht = einlesen.nextDouble();
	    eingabe="Als letztes geben Sie bitte Ihr Geschlecht an M/W: ";
	    System.out.print(eingabe);
	    geschlecht = einlesen.next();
	    
	    
	    kilogramm_bmi = kilogramm/100;
	    bmi = gewicht/(kilogramm_bmi*kilogramm_bmi);
	    
	    if (geschlecht.contains("M")){
	      if (bmi<20){
	        ausgabe="Sie haben Untergewicht " + bmi;
	      }
	      else if(bmi>=20 && bmi<=25){
	        ausgabe="Sie haben Normalgewicht " + bmi;
	      }
	      else{
	        ausgabe="Sie haben Übergewicht " + bmi;
	      }
	    }
	    else if (geschlecht.contains("W")){
	      if (bmi<19){
	        ausgabe="Sie haben Untergewicht " + bmi;
	      }
	      else if(bmi>=19 && bmi<=24){
	        ausgabe="Sie haben Normalgewicht " + bmi;
	      }
	      else{
	        ausgabe="Sie haben Übergewicht " + bmi;
	      }
	    }
	    else{
	      ausgabe="Das ist kein Geschlecht";
	    }
	    
	    System.out.println("");
	    System.out.println(ausgabe);    
	    
	    
	  }
	
	  

	}


