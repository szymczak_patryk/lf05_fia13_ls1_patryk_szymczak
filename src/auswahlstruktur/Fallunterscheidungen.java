package auswahlstruktur;

import java.util.Scanner;

public class Fallunterscheidungen {
	public static void main(String[] args) {
	 Scanner tastatur = new Scanner(System.in);
	 
	 System.out.print("Bitte geben Sie eine Note ein: ");
	 int note = tastatur.nextInt();
	 
	 
	 if (note == 1)
	     System.out.println("Sehr gut");
	 
	 else if (note == 2)
	     System.out.println("Gut");
	 
	 else if (note == 3)
		 System.out.println("Befriedigend");
	 
	 else if (note == 4)
		 System.out.println("Ausreichend");
	 
	 else if (note == 5)
		 System.out.println("Mangelhaft");
	 
	 else if  (note == 6)
		 System.out.println("Ungenugend");
	 
		
	 tastatur.close();
		
	}
}
