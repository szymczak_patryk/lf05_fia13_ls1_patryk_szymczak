package auswahlstruktur;

public class Schleifen1_Aufgabe5 {

	public static void main(String[] args) {
		  
	    int z, sp;
	    
	    for(z = 1; z <= 10; z++) {
	      
	      for(sp = 1; sp <= 10; sp++) {
	        
	        System.out.printf("%5d", z * sp);
	      }
	      
	      System.out.print("\n");
	    }
	 } 
}


