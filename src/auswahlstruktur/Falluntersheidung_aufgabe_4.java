package auswahlstruktur;

import java.util.Scanner;

public class Falluntersheidung_aufgabe_4 {

	public static void main(String[] args) {
	    
		 
	    char op = ' ';
	    double zahl1, zahl2, erg = 0.0;
		
		Scanner tastatur = new Scanner (System.in);
	      
	      
	      System.out.print("Bitte geben Sie die erste Zahl ein: ");
	      zahl1 = tastatur.nextDouble();
	      
	      System.out.print("Bitte eben Sie die zweite Zahl ein: ");
	      zahl2 = tastatur.nextDouble();
	      
	      System.out.print("Was soll berechnet werden. ");
	      System.out.println("Bitte geben Sie einer der folgenden Zeichen ein: ");
	      
	      System.out.println("(+) Addition");
	      System.out.println("(-) Subtraktion");
	      System.out.println("(*) Multiplikation");
	      System.out.println("(/) Division");
	      op = tastatur.next().charAt(0);
	      
	      if (op == '+') {
	        erg = zahl1 + zahl2;
	        System.out.println(zahl1 + " " + op + " " + zahl2 + " = " + Math.round(erg * 100)/100.0 );
	      } 
	      else if (op == '-') {
	          erg = zahl1 - zahl2;
	          System.out.println(zahl1 + " " + op + " " + zahl2 + " = " + Math.round(erg * 100)/100.0 );
	      }
	      else if (op == '*') {
	            erg = zahl1 * zahl2;
	            System.out.println(zahl1 + " " + op + " " + zahl2 + " = " + Math.round(erg * 100)/100.0 );
	      }
	      else if (op == '/') {
	               erg = zahl1 / zahl2;
	              System.out.println(zahl1 + " " + op + " " + zahl2 + " = " + Math.round(erg * 100)/100.0 );
	      }
	      else {
	        System.out.println("Sie haben keinen gültigen Operator eingegeben!!");
	      }
		
		}

}
